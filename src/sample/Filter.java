package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.model.Person;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Created by Litv on 25.03.17.
 */
public class Filter implements Initializable {

    private Stage dialogStage;

    @FXML
    private TableView<Person> tableFilter;

    public ObservableList<Person> list;

    @FXML
    private ComboBox<String> depComBox;
    @FXML
    private ComboBox<String> posComBox;
    @FXML
    private ComboBox<String> eduComBox;
    @FXML
    private ComboBox<Integer> expComBox;
    @FXML
    private ComboBox<String> maritalComBox;
    @FXML
    private ComboBox<Integer> salaryComBox;

    @FXML
    private TableColumn<Person, String> name;
    @FXML
    private TableColumn<Person, String> surname;
    @FXML
    private TableColumn<Person, String> position;
    @FXML
    private TableColumn<Person, String> department;
    @FXML
    private TableColumn<Person, String> education;
    @FXML
    private TableColumn<Person, Integer> workExp;
    @FXML
    private TableColumn<Person, String> marital;
    @FXML
    private TableColumn<Person, Integer> salary;

    public void initTableFilter () {

            if (depComBox.getValue() != null && posComBox.getValue() == null && eduComBox.getValue() == null &&
                    expComBox.getValue() == null && maritalComBox.getValue() == null && salaryComBox.getValue() == null) {
                list = getObservableListDepartments();
                tableFilter.setItems(list);
                // Проверка на ДОЛЖНОСТЬ
            } else if (depComBox.getValue() == null && posComBox.getValue() != null && eduComBox.getValue() == null &&
                    expComBox.getValue() == null && maritalComBox.getValue() == null && salaryComBox.getValue() == null) {
                list = getObservableListPosition();
                tableFilter.setItems(list);
                // Проверка на ОБРАЗОВАНИЕ
            } else if (depComBox.getValue() == null && posComBox.getValue() == null && eduComBox.getValue() != null &&
                    expComBox.getValue() == null && maritalComBox.getValue() == null && salaryComBox.getValue() == null) {
                list = getObservableListEducation();
                tableFilter.setItems(list);
                // Проверка на СТАЖ
            } else if (depComBox.getValue() == null && posComBox.getValue() == null && eduComBox.getValue() == null &&
                    expComBox.getValue() != null && maritalComBox.getValue() == null && salaryComBox.getValue() == null) {
                list = getObservableListExcp();
                tableFilter.setItems(list);
                // Проверка на СЕМЕЙНОЕ ПОЛОЖЕНИЕ
            } else if (depComBox.getValue() == null && posComBox.getValue() == null && eduComBox.getValue() == null &&
                    expComBox.getValue() == null && maritalComBox.getValue() != null && salaryComBox.getValue() == null) {
                list = getObservableListMarital();
                tableFilter.setItems(list);
                // Проверка на ЗАРПЛАТУ
            } else if (depComBox.getValue() == null && posComBox.getValue() == null && eduComBox.getValue() == null &&
                    expComBox.getValue() == null && maritalComBox.getValue() == null && salaryComBox.getValue() != null) {
                list = getObservableListSalary();
                tableFilter.setItems(list);
                // ОТДЕЛ И ДОЛЖНОСТЬ
            } else {
                list = getList();
                tableFilter.setItems(list);
            }
    }

    private ObservableList<Person> getObservableListPosition() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsInPositionFromdDB(posComBox.getValue());
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    private ObservableList<Person> getObservableListDepartments() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsInDeparmentFromdDB(depComBox.getValue());
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    private ObservableList<Person> getObservableListEducation() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsInEducationFromdDB(eduComBox.getValue());
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    private ObservableList<Person> getObservableListExcp() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsInExcpFromdDB(expComBox.getValue());
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    private ObservableList<Person> getObservableListMarital() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsInMaritalFromdDB(maritalComBox.getValue());
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    private ObservableList<Person> getObservableListSalary() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsInSalaryFromdDB(salaryComBox.getValue());
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    private ObservableList<Person> getList() {
        ArrayList<Person> people = SQLiteDatabase.getListFromdDatabase(depComBox, posComBox,
                eduComBox, expComBox, maritalComBox, salaryComBox);
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setData(){
        depComBox.getItems().clear();
        depComBox.getItems().addAll(SQLiteDatabase.getDepartments());
        posComBox.getItems().clear();
        posComBox.getItems().addAll(SQLiteDatabase.getPositions());
        eduComBox.getItems().clear();
        eduComBox.getItems().addAll(SQLiteDatabase.getEducation());
        expComBox.getItems().clear();
        expComBox.getItems().addAll(SQLiteDatabase.getExcp());
        maritalComBox.getItems().clear();
        maritalComBox.getItems().addAll(SQLiteDatabase.getMarital());
        salaryComBox.getItems().clear();
        salaryComBox.getItems().addAll(SQLiteDatabase.getSalary());



        name = new TableColumn<Person, String>("Имя");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        surname = new TableColumn<Person, String>("Фамилия");
        surname.setCellValueFactory(new PropertyValueFactory<>("surName"));
        position = new TableColumn<Person, String>("Должность");
        position.setCellValueFactory(new PropertyValueFactory<>("position"));
        department = new TableColumn<Person, String>("Отдел");
        department.setCellValueFactory(new PropertyValueFactory<>("department"));
        education = new TableColumn<Person, String>("Образование");
        education.setCellValueFactory(new PropertyValueFactory<>("education"));
        workExp = new TableColumn<Person, Integer>("Стаж");
        workExp.setCellValueFactory(new PropertyValueFactory<>("workExp"));
        marital = new TableColumn<Person, String>("Семейное положение");
        marital.setCellValueFactory(new PropertyValueFactory<>("marital"));
        salary = new TableColumn<Person, Integer>("Зарплата");
        salary.setCellValueFactory(new PropertyValueFactory<>("salary"));

        tableFilter.getColumns().setAll(name, surname, position, department, education, workExp, marital, salary);
    }
}
