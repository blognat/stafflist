package sample.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {

    private StringProperty id;
    private StringProperty name;
    private StringProperty surName;
    private StringProperty position;
    private StringProperty department;
    private StringProperty education;
    private IntegerProperty workExp;
    private StringProperty marital;
    private IntegerProperty salary;

    public Person(){}


    public Person(String id, String name, String surName, String position, String department, String education, int workExp,
                  String marital, int salary) {
        this.id = new SimpleStringProperty(id);
        this.name = new SimpleStringProperty(name);
        this.surName = new SimpleStringProperty(surName);
        this.position = new SimpleStringProperty(position);
        this.department = new SimpleStringProperty(department);
        this.education = new SimpleStringProperty(education);
        this.workExp = new SimpleIntegerProperty(workExp);
        this.marital = new SimpleStringProperty(marital);
        this.salary = new SimpleIntegerProperty(salary);
    }

    public String getId() {
        return id.get();
    }

    public StringProperty idProperty() {
        return id;
    }

    public void setId(String id) {
        this.id.set(id);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getSurName() {
        return surName.get();
    }

    public StringProperty surNameProperty() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName.set(surName);
    }

    public String getPosition() {
        return position.get();
    }

    public StringProperty positionProperty() {
        return position;
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public String getDepartment() {
        return department.get();
    }

    public StringProperty departmentProperty() {
        return department;
    }

    public void setDepartment(String department) {
        this.department.set(department);
    }

    public String getEducation() {
        return education.get();
    }

    public StringProperty educationProperty() {
        return education;
    }

    public void setEducation(String education) {
        this.education.set(education);
    }

    public int getWorkExp() {
        return workExp.get();
    }

    public IntegerProperty workExpProperty() {
        return workExp;
    }

    public void setWorkExp(int workExp) {
        this.workExp.set(workExp);
    }

    public String getMarital() {
        return marital.get();
    }

    public StringProperty maritalProperty() {
        return marital;
    }

    public void setMarital(String marital) {
        this.marital.set(marital);
    }

    public int getSalary() {
        return salary.get();
    }

    public IntegerProperty salaryProperty() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary.set(salary);
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surName='" + surName + '\'' +
                ", position='" + position + '\'' +
                ", department='" + department + '\'' +
                ", education='" + education + '\'' +
                ", workExp=" + workExp +
                ", marital=" + marital +
                ", salary=" + salary +
                '}';
    }
}
