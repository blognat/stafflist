package sample;
import javafx.scene.control.ComboBox;
import sample.model.Person;

import java.sql.*;
import java.sql.Connection;
import java.util.ArrayList;

public class SQLiteDatabase {

    public static final String POSITION = "position";
    public static final String POSITION_VALUE = "pos";
    public static final String DEPARTMENT = "department";
    public static final String DEPARTMENT_VALUE = "depart";
    public static final String MARITAL = "marital";
    public static final String MARITAL_VALUE = "marr";
    public static final String EDUCATION = "education";
    public static final String EDUCATION_VALUE = "educate";


    /**
     * Метод - коннектор к базе данных
     */
    public static Connection connectionSQLite(){

        Connection c = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:data.db");
        } catch ( Exception e ) {
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
            System.exit(0);
        }
        System.out.println("Opened database successfully");

        return c;
    }

    public static void creatTable(String script){
        try {
            Statement statement = connectionSQLite().createStatement();
            statement.executeUpdate(script);
            System.out.println("Таблица создана");
            statement.close();
            connectionSQLite().close();
            System.out.println("Соединения закрыты");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Вставка строки
     * Метод получает на вход значения объекта Person и вставляет их в базу данных
     * @auth Litv
     * @param id
     * @param name
     * @param surName
     * @param position
     * @param department
     * @param education
     * @param workExp
     * @param marital
     * @param salary
     */
    public static void insertPerson(String id, String name, String surName, String position, String department,
                                    String education, int workExp, String marital, int salary) {
        try {
            Statement statement = connectionSQLite().createStatement();

            statement.executeUpdate("INSERT INTO persons (idUUID, name, surname, position, department, education, " +
                        "workExp, marital, salary) " +
                        "VALUES (\'" + id + "\', \'" + name + "\', \'" + surName + "\', \'" + position + "\', \'" + department + "\', \'" +
                    education + "\', \'" + workExp + "\', \'" + marital + "\', \'" + salary + "\')");

            statement.close();
            connectionSQLite().close();
            System.out.println("Соединения закрыты");
            }catch (SQLException e) {
                e.printStackTrace();
        }
    }

    /**
     * Обновление строки
     * Метод получает значения для объекта Person из таблицы и обвляет их, перезаписывая как в талице, так и
     * в базе данных
     * @auth Litv
     * @param id
     * @param name
     * @param surName
     * @param position
     * @param department
     * @param education
     * @param workExp
     * @param marital
     * @param salary
     */
    public static void updatePerson(String id, String name, String surName, String position, String department,
                                    String education, int workExp, String marital, int salary){
        try{
            Statement statement = connectionSQLite().createStatement();
            statement.executeUpdate("UPDATE persons SET " +
                    "name = \'" + name + "\', " +
                    "surname = \'" + surName + "\', " +
                    "position = \'" + position + "\', " +
                    "department = \'" + department + "\', " +
                    "education = \'" + education + "\', " +
                    "workExp = " + workExp + ", " +
                    "marital = \'" + marital + "\', " +
                    "salary = " + salary + " WHERE idUUID = \'" + id + "\'");
            statement.close();
            connectionSQLite().close();
            System.out.println("Соединения закрыты");
        }  catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод удаления из базы данных и таблицы
     * @param id
     */
    public static void deleteRowPerson(String id){
        try {
            Statement statement = connectionSQLite().createStatement();
            statement.executeUpdate("DELETE FROM persons WHERE idUUID = \'" + id+"\'");

            statement.close();
            connectionSQLite().close();
            System.out.println("Соединения закрыты");
        }  catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // -------- Чтение таблицы построчно ------
    public static ArrayList<Person> getPersonsFromdDB(){
        ArrayList<Person> people = new ArrayList<>();
        try{
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String position = resultSet.getString("position");
                String department = resultSet.getString("department");
                String education = resultSet.getString("education");
                int workExp = resultSet.getInt("workExp");
                String marital = resultSet.getString("marital");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
            System.out.println("Соединения закрыты");
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static String createTeblePersonsSCRIPT =
            "CREATE TABLE IF NOT EXISTS persons " +
                    "(idUUID          TEXT    NOT NULL, " +
                    "name         TEXT    NOT NULL, " +
                    "surname      TEXT    NOT NULL, " +
                    "position     TEXT    NOT NULL, " +
                    "department   TEXT    NOT NULL, " +
                    "education    TEXT    NOT NULL, " +
                    "workExp      INTEGER NOT NULL, " +
                    "marital      TEXT    NOT NULL, " +
                    "salary       INTEGER NOT NULL)";

    public static String createTebleEducationSCRIPT =
            "CREATE TABLE IF NOT EXISTS " + EDUCATION +
                    "("+ EDUCATION_VALUE + "   TEXT    NOT NULL)";

    public static String createTebleMaritalSCRIPT =
            "CREATE TABLE IF NOT EXISTS " + MARITAL + "(" + MARITAL_VALUE + "  TEXT  NOT NULL)";

    public static String createTebleDepartmentSCRIPT =
            "CREATE TABLE IF NOT EXISTS " + DEPARTMENT +
                    "(" + DEPARTMENT_VALUE + "  TEXT  NOT NULL)";

    public static String createTeblePositiontSCRIPT =
            "CREATE TABLE IF NOT EXISTS " + POSITION +
                    "(" + POSITION_VALUE + "  TEXT  NOT NULL)";

    public static void insertValues(String tableName, String column, String value) {
        try {
            Statement statement = connectionSQLite().createStatement();

            statement.executeUpdate("INSERT INTO " + tableName + " (" + column + ") " +
                    "VALUES (\'" + value + "\')");

            statement.close();
            connectionSQLite().close();
            System.out.println("Соединения закрыты");
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setData(){
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "Младший специалист");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "Старший специалист");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "Ведущий специалист");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "Программист");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "SEO-специалист");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE,  "SMM-менеджер");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "Бухгалтер");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE, "Заместитель начальника");
        insertValues(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE,  "Начальник");

        insertValues(SQLiteDatabase.DEPARTMENT, SQLiteDatabase.DEPARTMENT_VALUE, "SEO");
        insertValues(SQLiteDatabase.DEPARTMENT, SQLiteDatabase.DEPARTMENT_VALUE,  "Бухгалтерия");
        insertValues(SQLiteDatabase.DEPARTMENT, SQLiteDatabase.DEPARTMENT_VALUE,  "Рекламный");
        insertValues(SQLiteDatabase.DEPARTMENT, SQLiteDatabase.DEPARTMENT_VALUE,  "Технический");
        insertValues(SQLiteDatabase.DEPARTMENT, SQLiteDatabase.DEPARTMENT_VALUE,  "Разработка ПО");

        insertValues(SQLiteDatabase.MARITAL, SQLiteDatabase.MARITAL_VALUE, "Женат");
        insertValues(SQLiteDatabase.MARITAL, SQLiteDatabase.MARITAL_VALUE, "Холост");
        insertValues(SQLiteDatabase.MARITAL, SQLiteDatabase.MARITAL_VALUE, "Замужем");
        insertValues(SQLiteDatabase.MARITAL, SQLiteDatabase.MARITAL_VALUE, "Не замужем");

        insertValues(SQLiteDatabase.EDUCATION,  SQLiteDatabase.EDUCATION_VALUE, "Высшее");
        insertValues(SQLiteDatabase.EDUCATION,  SQLiteDatabase.EDUCATION_VALUE, "Среднее профессиональное");
        insertValues(SQLiteDatabase.EDUCATION,  SQLiteDatabase.EDUCATION_VALUE, "Среднее общее");
    }


    public static ArrayList<String> getValuesFromTable(String table, String value){
        ArrayList<String> people = new ArrayList<>();
        String val = value;
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT " + val + " FROM " + table);
            while (resultSet.next()){
                String result = resultSet.getString(val);
                people.add(result);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }


    public static ArrayList<Person> getPersonsInDeparmentFromdDB(String department) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE department = \'" + department + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String position = resultSet.getString("position");
                String street = resultSet.getString("education");
                int workExp = resultSet.getInt("workExp");
                String marital = resultSet.getString("marital");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, street, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static ArrayList<Person> getPersonsInEducationFromdDB(String education) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE education = \'" + education + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String position = resultSet.getString("position");
                String department = resultSet.getString("department");
                int workExp = resultSet.getInt("workExp");
                String marital = resultSet.getString("marital");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }


    public static ArrayList<Person> getPersonsInPositionFromdDB(String position) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE position = \'" + position + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String department = resultSet.getString("department");
                String education = resultSet.getString("education");
                int workExp = resultSet.getInt("workExp");
                String marital = resultSet.getString("marital");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static ArrayList<Person> getPersonsInExcpFromdDB(Integer workExp) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE workExp = \'" + workExp + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String department = resultSet.getString("department");
                String education = resultSet.getString("education");
                String position = resultSet.getString("position");
                String marital = resultSet.getString("marital");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }


    public static ArrayList<Person> getPersonsInSalaryFromdDB(Integer salary) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE salary = \'" + salary+ "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String department = resultSet.getString("department");
                String education = resultSet.getString("education");
                String position = resultSet.getString("position");
                String marital = resultSet.getString("marital");
                int workExp = resultSet.getInt("workExp");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static ArrayList<Person> getPersonsInMaritalFromdDB(String marital) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE marital = \'" + marital + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String department = resultSet.getString("department");
                String education = resultSet.getString("education");
                String position = resultSet.getString("position");
                int workExp = resultSet.getInt("workExp");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static ArrayList<Person> getPersonsInPositionAndDepartmentFromdDB(String position, String department) {
        ArrayList<Person> people = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE position = \'" + position + "\' AND department = \'" +
                    department + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");
                String education = resultSet.getString("education");
                String marital = resultSet.getString("marital");
                int workExp = resultSet.getInt("workExp");
                int salary = resultSet.getInt("salary");
                people.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }  catch (SQLException e) {
            e.printStackTrace();
        }
        return people;
    }

    public static ArrayList<String> getPositions() {
        ArrayList<String> positions = new ArrayList<>();
        positions.add(null);
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT position FROM persons");
            while (resultSet.next()){
                String pos = resultSet.getString("position");
                positions.add(pos);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return positions;
    }

    public static ArrayList<String> getDepartments() {
        ArrayList<String> department = new ArrayList<>();
        department.add(null);
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT department FROM persons");
            while (resultSet.next()){
                String dep = resultSet.getString("department");
                department.add(dep);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return department;
    }

    public static ArrayList<String> getEducation() {
        ArrayList<String> education = new ArrayList<>();
        education.add(null);
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT education FROM persons");
            while (resultSet.next()){
                String edu = resultSet.getString("education");
                education.add(edu);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return education;
    }

    public static ArrayList<Integer> getExcp() {
        ArrayList<Integer> exps = new ArrayList<>();
        exps.add(null);
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT workExp FROM persons");
            while (resultSet.next()){
                int exp = resultSet.getInt("workExp");
                exps.add(exp);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return exps;
    }

    public static ArrayList<Integer> getSalary() {
        ArrayList<Integer> salaryies = new ArrayList<>();
        salaryies.add(null);
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT salary FROM persons");
            while (resultSet.next()){
                int sal = resultSet.getInt("salary");
                salaryies.add(sal);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return salaryies;
    }

    public static ArrayList<String> getMarital() {
        ArrayList<String> maritalList = new ArrayList<>();
        maritalList.add(null);
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT DISTINCT marital FROM persons");
            while (resultSet.next()){
                String mar = resultSet.getString("marital");
                maritalList.add(mar);
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return maritalList;
    }


    public static ArrayList<Person> getListFromdDB(String dep, String pos, String edu, int exp, String mar, int sal) {
        ArrayList<Person> pers = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM persons WHERE department = \'" + dep + "\' AND" +
            " position = \'" + pos + "\' AND" + " education = \'" + edu + "\' AND workExp = \'" + exp + "\' AND" +
                    " marital = \'" + mar + "\'" + " AND salary = \'" + sal + "\'");
            while (resultSet.next()){
                String id = resultSet.getString("idUUID");
                String name = resultSet.getString("name");
                String surName = resultSet.getString("surname");

                pers.add(new Person(id, name, surName, pos, dep, edu, exp, mar, sal));
            }
            resultSet.close();
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return pers;
    }

    /**
     * Метод принимает на вход значения из пунктов фильтра. Проверяет, что выбрано и, в зависимости от этого
     * выбирает запрос для поиска
     * @param dep
     * @param pos
     * @param edu
     * @param exp
     * @param mar
     * @param sal
     * @return
     */

    public static ArrayList<Person> getListFromdDatabase(ComboBox<String> dep, ComboBox<String> pos,
                                                         ComboBox<String> edu, ComboBox<Integer> exp,
                                                         ComboBox<String> mar, ComboBox<Integer> sal) {
        ArrayList<Person> pers = new ArrayList<>();
        try {
            Statement statement = connectionSQLite().createStatement();
            ResultSet resultSet;
            String id, name, surName, position, department, education, marital;
            int workExp, salary;
            // Поиск по всем
            if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() != null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка отдел + должность
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() == null && exp.getValue() == null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка отдел + образование
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка отдел + опыт
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() == null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка отдел + семейное положение
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() == null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка отдел + зарплата
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() == null && exp.getValue() == null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка должность + образование
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка должность + опыт
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() == null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка должность + семейное положение
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() == null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Проверка должность + зарплата
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() == null && exp.getValue() == null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // образование + опыт
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // образование + семейное положение
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() != null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Образование + зарплата
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // опыт + семейное положение
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() == null && exp.getValue() != null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Опыт + зарплата
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() == null && exp.getValue() != null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Семейное положение + зарплата
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() == null && exp.getValue() == null && mar.getValue() != null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " marital = \'" + mar.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, должность, образование
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, должность, опыт
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() == null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, должность, семейное положение
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() == null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, должность, зарплата
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() == null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, образование, опыт
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, образование, семейное положение
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() != null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, образование, зарплата
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, опыт, семейное положение
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() == null && exp.getValue() != null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, опыт, зарплата
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() == null && exp.getValue() != null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, семейное положение, зарплата
            } else if (dep.getValue() != null && pos.getValue() == null && edu.getValue() == null && exp.getValue() == null && mar.getValue() != null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // должность, образовани, опыт
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Должность, образование, семейное положение
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Должность, образование, зарплата
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Образование, опыт, семейное положение
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() != null && exp.getValue() != null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Образование, опыт, зарплата
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // опыт, семейное положение, зарплата
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() == null && exp.getValue() != null && mar.getValue() != null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // отдел, должность, образование, опыт
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // отдел, должность, образование, семейное положение
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() == null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, должность, образование, зарплата
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() == null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // должность, образование, опыт, семейное положение
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Должность, образование, опыт, зарплата
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // образование, опыт, семейное положение, зарплата
            } else if (dep.getValue() == null && pos.getValue() == null && edu.getValue() != null && exp.getValue() != null && mar.getValue() != null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // Отдел, должность, образование, опыт, семейное положение
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() != null && sal.getValue() == null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // отдел, должность, образование, опыт, зарплата
            } else if (dep.getValue() != null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() == null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " department = \'" + dep.getValue() + "\' AND" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");

                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
                // должность, образование, опыт, семейное положение, зарплата
            } else if (dep.getValue() == null && pos.getValue() != null && edu.getValue() != null && exp.getValue() != null && mar.getValue() != null && sal.getValue() != null) {
                resultSet = statement.executeQuery("SELECT * FROM persons WHERE" +
                        " position = \'" + pos.getValue() + "\' AND" +
                        " education = \'" + edu.getValue() + "\' AND" +
                        " workExp = \'" + exp.getValue() + "\' AND" +
                        " marital = \'" + mar.getValue() + "\' AND" +
                        " salary = \'" + sal.getValue() + "\'");
                while (resultSet.next()) {
                    id = resultSet.getString("idUUID");
                    name = resultSet.getString("name");
                    surName = resultSet.getString("surname");
                    position = resultSet.getString("position");
                    department = resultSet.getString("department");
                    education = resultSet.getString("education");
                    workExp = resultSet.getInt("workExp");
                    marital = resultSet.getString("marital");
                    salary = resultSet.getInt("salary");
                    pers.add(new Person(id, name, surName, position, department, education, workExp, marital, salary));
                }
                resultSet.close();
            }
            statement.close();
            connectionSQLite().close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return pers;
    }
}
