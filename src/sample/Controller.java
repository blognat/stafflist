package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import jdk.nashorn.internal.runtime.regexp.joni.SearchAlgorithm;
import sample.model.Person;

import java.util.ArrayList;
import java.util.UUID;

public class Controller {

    private Stage dialogStage;

    public ObservableList<Person> observableList;
    public ObservableList<Person> observableListPostion;
    public ObservableList<Person> observableListDepartment;
    public ObservableList<Person> observableListEducation;
    public ObservableList<Person> observableListMarital;

    @FXML
    private TableView<Person> table;

    @FXML
    private TableColumn<Person, String> name;
    @FXML
    private TableColumn<Person, String> surname;
    @FXML
    private TableColumn<Person, String> position;
    @FXML
    private TableColumn<Person, String> department;
    @FXML
    private TableColumn<Person, String> education;
    @FXML
    private TableColumn<Person, Integer> workExp;
    @FXML
    private TableColumn<Person, String> marital;
    @FXML
    private TableColumn<Person, Integer> salary;
    @FXML
    private TextField nameStr;
    @FXML
    private TextField surnameStr;
    @FXML
    private ComboBox<String> positionStr;
    @FXML
    private ComboBox<String> departmentStr;
    @FXML
    private ComboBox<String> educationStr;
    @FXML
    private TextField workExpInt;
    @FXML
    private ComboBox<String> maritalStr;
    @FXML
    private TextField salaryInt;

    // -- Загрузка данных в таблицу при открытии приложения ---
    public void initTable() {

        observableList = getObservableList();
        table.setItems(observableList);

        positionStr.getItems().clear();
        positionStr.getItems().addAll(SQLiteDatabase.getValuesFromTable(SQLiteDatabase.POSITION, SQLiteDatabase.POSITION_VALUE));
        departmentStr.getItems().clear();
        departmentStr.getItems().addAll(SQLiteDatabase.getValuesFromTable(SQLiteDatabase.DEPARTMENT, SQLiteDatabase.DEPARTMENT_VALUE));
        educationStr.getItems().clear();
        educationStr.getItems().addAll(SQLiteDatabase.getValuesFromTable(SQLiteDatabase.EDUCATION, SQLiteDatabase.EDUCATION_VALUE));
        maritalStr.getItems().clear();
        maritalStr.getItems().addAll(SQLiteDatabase.getValuesFromTable(SQLiteDatabase.MARITAL, SQLiteDatabase.MARITAL_VALUE));

        name = new TableColumn<Person, String>("Имя");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        surname = new TableColumn<Person, String>("Фамилия");
        surname.setCellValueFactory(new PropertyValueFactory<>("surName"));
        position = new TableColumn<Person, String>("Должность");
        position.setCellValueFactory(new PropertyValueFactory<>("position"));
        department = new TableColumn<Person, String>("Отдел");
        department.setCellValueFactory(new PropertyValueFactory<>("department"));
        education = new TableColumn<Person, String>("Образование");
        education.setCellValueFactory(new PropertyValueFactory<>("education"));
        workExp = new TableColumn<Person, Integer>("Стаж");
        workExp.setCellValueFactory(new PropertyValueFactory<>("workExp"));
        marital = new TableColumn<Person, String>("Семейное положение");
        marital.setCellValueFactory(new PropertyValueFactory<>("marital"));
        salary = new TableColumn<Person, Integer>("Зарплата");
        salary.setCellValueFactory(new PropertyValueFactory<>("salary"));

        table.getColumns().setAll(name, surname, position, department, education, workExp, marital, salary);

//        table.setRowFactory(tv -> {
//            TableRow<Person> row = new TableRow<>();
//            row.setOnMouseClicked(event -> {
//            if (event.getClickCount() == 1 && !row.isEmpty()){
//                Person person = table.getSelectionModel().getSelectedItem();
//                nameStr.setText(person.getName());
//                surnameStr.setText(person.getSurName());
//                positionStr.setText(person.getPosition());
//                departmentStr.setText(person.getDepartment());
//                educationStr.setText(person.getEducation());
//                workExpInt.setText(String.valueOf(person.getWorkExp()));
//                maritalStr.setText(person.getMarital());
//                salaryInt.setText(String.valueOf(person.getSalary()));
//            }
//        });
//            return row;
//        });
    }

    // ----- Получение списка ObservableList<Person> data для добавления в таблицу --------
    public ObservableList<Person> getObservableList() {
        ArrayList<Person> people = SQLiteDatabase.getPersonsFromdDB();
        ObservableList<Person> data = FXCollections.observableArrayList(people);
        return data;
    }


    public ObservableList<String> getValues(ArrayList<String> list){
        ObservableList<String> data = FXCollections.observableArrayList(list);
        return data;
    }

    // ----- Метод добавления в таблицу и базу данных строки
    public void addPerson() {
        if (isInputValid()) {
            String id = UUID.randomUUID().toString();
            String nameS = nameStr.getText();
            String surnameS = surnameStr.getText();
            String positionS = positionStr.getValue();
            String departmentS = departmentStr.getValue();
            String educationS = educationStr.getValue();
            Integer workExpS = Integer.parseInt(workExpInt.getText());
            String maritalS = maritalStr.getValue();
            Integer salaryS = Integer.parseInt(salaryInt.getText());

            SQLiteDatabase.insertPerson(id, nameS, surnameS, positionS, departmentS, educationS, workExpS, maritalS, salaryS);
            observableList.add(new Person(id, nameS, surnameS, positionS, departmentS, educationS, workExpS, maritalS, salaryS));
            System.out.println("Сотрудник добавлен");

            nameStr.clear();
            surnameStr.clear();
            workExpInt.clear();
            salaryInt.clear();
        }

        table.getItems().clear();
        table.setItems(getObservableList());
    }


    // ----- Метод удаления из таблицы и базы данных строки
    public void delPerson() {
        if (isFocusValid()) {
            Person person = table.getSelectionModel().getSelectedItem();
            String id = person.getId();
            SQLiteDatabase.deleteRowPerson(id);
            observableList.remove(person);
        }
        table.getItems().clear();
        table.setItems(getObservableList());
    }

    // ----- Метод обновления данных строки в таблице и базе данных
    public void updPerson(){
        if (isFocusValid()) {
            Person person = table.getSelectionModel().getSelectedItem();
            String id = person.getId();
            if (isInputValid()) {
                String nameS = nameStr.getText();
                String surnameS = surnameStr.getText();
                String positionS = positionStr.getValue();
                String departmentS = departmentStr.getValue();
                String educationS = educationStr.getValue();
                Integer workExpS = Integer.parseInt(workExpInt.getText());
                String maritalS = maritalStr.getValue();
                Integer salaryS = Integer.parseInt(salaryInt.getText());

                SQLiteDatabase.updatePerson(id, nameS, surnameS, positionS, departmentS, educationS, workExpS, maritalS, salaryS);

                table.getItems().clear();
                table.setItems(getObservableList());
            }
        }

    }

    // ----- Метод для очистки полей ввода данных
    public void clearText(){
        nameStr.clear();
        surnameStr.clear();
        workExpInt.clear();
        salaryInt.clear();
    }


    private boolean isInputValid() {
        String errorMessage = "";

        if (nameStr.getText() == null || nameStr.getText().length() == 0) {
            errorMessage += "Введите имя!\n";
        }
        if (surnameStr.getText() == null || surnameStr.getText().length() == 0) {
            errorMessage += "Введите фамилию!\n";
        }
        if (positionStr.getValue()== null || positionStr.getValue().length() == 0) {
            errorMessage += "Введите должность!\n";
        }

        if (educationStr.getValue() == null || educationStr.getValue().length() == 0) {
            errorMessage += "Введите улицу!\n";
        }

        if (departmentStr.getValue() == null || departmentStr.getValue().length() == 0) {
            errorMessage += "Введите город!\n";
        }

        if (maritalStr.getValue() == null || maritalStr.getValue().length() == 0) {
            errorMessage += "Введите день рождения!\n";
        }

        if (workExpInt.getText() == null || workExpInt.getText().length() == 0) {
            errorMessage += "Введите номер дома!\n";
        } else {
            // пытаемся преобразовать зарплату в int.
            try {
                Integer.parseInt(workExpInt.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Неправильный формат в поле \"Номер дома\"!\n";
            }
        }

        if (salaryInt.getText() == null || salaryInt.getText().length() == 0) {
            errorMessage += "Ведите зарплату!\n";
        } else {
            // пытаемся преобразовать зарплату в int.
            try {
                Integer.parseInt(salaryInt.getText());
            } catch (NumberFormatException e) {
                errorMessage += "Неправильный формат поля \"Зарплата\"!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Неверное значение");
            alert.setHeaderText("Пожалуйста, исправьте значение поля");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    private boolean isFocusValid() {
        String errorMessage = "";
        if (table.getSelectionModel().getSelectedItem() != null){
            return true;
        } else {
            // Показываем сообщение об ошибке.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Не выбран сотрудник");
            alert.setHeaderText("Дважды щелкните по сотруднику в таблице");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    public void loadFilterStage(){
        Main main = new Main();
        main.showFilterDialog();
    }

}
