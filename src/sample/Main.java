package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import java.io.IOException;


public class Main extends Application {

    private Stage primaryStage;
    private AnchorPane anchorPane;

    @Override
    public void start(Stage primaryStage) throws Exception{
        this.primaryStage = primaryStage;
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Main.class.getResource("sample.fxml"));
        anchorPane = (AnchorPane) loader.load();
        Scene scene = new Scene(anchorPane);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(scene);

        Controller controller = loader.getController();
        controller.initTable();

        SQLiteDatabase.creatTable(SQLiteDatabase.createTeblePersonsSCRIPT);
        SQLiteDatabase.creatTable(SQLiteDatabase.createTebleEducationSCRIPT);
        SQLiteDatabase.creatTable(SQLiteDatabase.createTebleMaritalSCRIPT);
        SQLiteDatabase.creatTable(SQLiteDatabase.createTebleDepartmentSCRIPT);
        SQLiteDatabase.creatTable(SQLiteDatabase.createTeblePositiontSCRIPT);

        SQLiteDatabase.setData();


        primaryStage.show();

    }

    public void showFilterDialog() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("filter.fxml"));

            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);

            Scene scene = new Scene(fxmlLoader.load());
            stage.setTitle("Фильтр");
            stage.setScene(scene);

            Filter filter = fxmlLoader.getController();
            filter.setData();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
